import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import twitter4j.FilterQuery;
import twitter4j.GeoLocation;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.ConfigurationBuilder;

public class Main {
	
	public static final String CONSUMER_KEY = "xLTqkBqY5goMgKgSQZM9xg";
	public static final String CONSUMER_SECRET = "PCn78UFzLlJxNFLnhE33GQ66G3DQGzZB9MG8qebWkTU";
	public static final String ACCESS_TOKEN = "1957980702-2ApQS5HTZQc349FgEpCv4wIomExl5Ly8KuZGmVz";
	public static final String ACCESS_SECRET = "usjC0sEscGM4xhDlcinUKNlEDAHz6NfctJ08N7D4Yiw";
	/**
	 * @param args
	 * @throws TwitterException 
	 * @throws IOException 
	 * @throws URISyntaxException 
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		try
		{
			//twitterAuthenticate(twitter);
			twitterSearch();
			//twitterStream();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private static void twitterAuthenticate(Twitter twitter) throws URISyntaxException, IOException, TwitterException {
		twitter.setOAuthConsumer(CONSUMER_KEY, CONSUMER_SECRET);
		RequestToken requestToken = twitter.getOAuthRequestToken();
		AccessToken accessToken = null;
		
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	    while (null == accessToken) {
	    	System.out.println("Trying to open the url");
	    	
	    	try {
	    		Desktop d = Desktop.getDesktop();		
	    		d.browse(new URI(requestToken.getAuthorizationURL()));
	    	} 
	    	catch (MalformedURLException e) { 
	    	    System.out.println("DOESNT UNDERSTAND THE URL");
	    	} 
	    	catch (IOException e) {   
	    		System.out.println("IO EXCEPTION");
	    	}
	      
	      System.out.print("Enter the PIN(if aviailable) or just hit enter.[PIN]:");
	      String pin = br.readLine();
	     
	      try{
	         if(pin.length() > 0){
	           accessToken = twitter.getOAuthAccessToken(requestToken, pin);
	         }else{
	           accessToken = twitter.getOAuthAccessToken();
	         }
	      } catch (TwitterException te) {
	        if(401 == te.getStatusCode()){
	          System.out.println("Unable to get the access token.");
	        }else{
	          te.printStackTrace();
	        }
	      }
	    }
	}
	
	private static void twitterStream() {
		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setOAuthAccessToken(ACCESS_TOKEN);
		cb.setOAuthAccessTokenSecret(ACCESS_SECRET);
		cb.setOAuthConsumerKey(CONSUMER_KEY);
		cb.setOAuthConsumerSecret(CONSUMER_SECRET);
		cb.setDebugEnabled(true);
		
		StatusListener listener = new StatusListener() {
	        public void onStatus(Status status) {
	        	if ((status.getPlace() != null) && (status.getGeoLocation() !=null)) {
	        		System.out.println("TWEET ID: " + status.getId());
		        	System.out.println("Place: " + status.getPlace().getName() + ", " + status.getPlace().getCountry());
		        	System.out.println("G-Location: " +status.getGeoLocation());
		            System.out.println(status.getUser().getName() + " : " + status.getText());
		            System.out.println("--------------------------------------------------");
	        	}
	        }
	        public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
	        	System.out.println("DELETION NOTICE FOR: " + statusDeletionNotice.getStatusId());
	        }
	        public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
	        	System.out.println("You're Being limited you fool");
	        }
	        public void onException(Exception ex) {
	            ex.printStackTrace();
	        }
			@Override
			public void onScrubGeo(long arg0, long arg1) { // do nothing
			}
			@Override
			public void onStallWarning(StallWarning arg0) { // do nothing
			}
	    };
	    
	    TwitterStream twitterStream = new TwitterStreamFactory(cb.build()).getInstance();
	    
	    FilterQuery fq = new FilterQuery();
	    String keywords[] = {"October", "#Swag"};

	    fq.track(keywords);
	    twitterStream.addListener(listener);
	    twitterStream.filter(fq);
	}
	
	private static void twitterSearch() throws TwitterException {
	
		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setOAuthAccessToken(ACCESS_TOKEN);
		cb.setOAuthAccessTokenSecret(ACCESS_SECRET);
		cb.setOAuthConsumerKey(CONSUMER_KEY);
		cb.setOAuthConsumerSecret(CONSUMER_SECRET);
		cb.setDebugEnabled(true);
		
		Twitter twitter = new TwitterFactory(cb.build()).getInstance();
		
	    Query query = new Query("Korean");
	    //GeoLocation geoloc = new GeoLocation(30, -110);
	    //query.setGeoCode(geoloc, 30,"mi");
	    //Unsure of this count as it may just set an upper limit and not force size
	    query.setCount(30);
		QueryResult results = twitter.search(query);
		List<Status> statuses = results.getTweets();
		System.out.println("Showing MEXICAN TWEETS timeline:");
		System.out.println("Size of status set is:" + statuses.size());
		 
		for (Status s : statuses) {
			System.out.println("["+s.getUser().getName() + "]  :  " + s.getText() + ", created at: " + s.getCreatedAt());
			System.out.println("Geolocation was: " + s.getGeoLocation());
			
			if(!(s.getPlace() == null))
			{
				System.out.println("Country was: " + s.getPlace().getCountry());
				System.out.println("Location Name is: " + s.getPlace().getName());
				System.out.println("Geo was :" + s.getPlace().getGeometryCoordinates());
			}

			System.out.println("------------------------------------------------------------------");
		}
		
	}

}
