package com.example.graphics;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;


public class ImageFeedActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_imagefeed);

        GridView gridView = (GridView) findViewById(R.id.grid_view);

        // Instance of ImageAdapter.java Class
        gridView.setAdapter(new ImageAdapter(this));

    }


}