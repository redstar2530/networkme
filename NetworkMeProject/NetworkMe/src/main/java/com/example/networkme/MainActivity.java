package com.example.networkme;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import twitter4j.FilterQuery;
import twitter4j.GeoLocation;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.ConfigurationBuilder;


public class MainActivity extends Activity {

    public static final String CONSUMER_KEY = "xLTqkBqY5goMgKgSQZM9xg";
    public static final String CONSUMER_SECRET = "PCn78UFzLlJxNFLnhE33GQ66G3DQGzZB9MG8qebWkTU";
    public static final String ACCESS_TOKEN = "1957980702-2ApQS5HTZQc349FgEpCv4wIomExl5Ly8KuZGmVz";
    public static final String ACCESS_SECRET = "usjC0sEscGM4xhDlcinUKNlEDAHz6NfctJ08N7D4Yiw";

    String[] textList = new String[5];
    List<Status> statusList = new ArrayList<Status>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView textListView = (ListView)findViewById(R.id.textFeed);
        ArrayAdapter<String> textAdapter = new ArrayAdapter<String>(this, R.layout.main_textfeed, R.id.texts, textList);
        textListView.setAdapter(textAdapter);


        twitterAsyncSearch twitterTask = new twitterAsyncSearch();
        twitterTask.execute();
        addTextList(statusList);

        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            filterSearch(query);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        // Get the SearchView and set the searchable configuration
        SearchManager mainSearchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView mainSearchView = (SearchView) findViewById(R.id.mainSearchBar);
        mainSearchView.setSearchableInfo(mainSearchManager.getSearchableInfo(getComponentName()));
        mainSearchView.setIconifiedByDefault(true);

        SearchManager locationSearchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView locationSearchView = (SearchView) findViewById(R.id.locationSearch);
        locationSearchView.setSearchableInfo(locationSearchManager.getSearchableInfo(getComponentName()));
        mainSearchView.setIconifiedByDefault(true);

        return true;
    }


    private void filterSearch(String query) {
        char c = query.charAt(0);
        if (c == '#') {
            TextView t = (TextView)findViewById(R.id.textFeed);
            t.setText(query);
        } else {
            TextView t = (TextView)findViewById(R.id.mapView);
            t.setText(query);
        }
    }

    private class twitterAsyncSearch extends AsyncTask<Void,Void,QueryResult> {

        private List<twitter4j.Status> tweetList = new ArrayList<twitter4j.Status>();

        @Override
        protected QueryResult doInBackground(Void... voids) {
            ConfigurationBuilder cb = new ConfigurationBuilder();
            cb.setOAuthAccessToken(ACCESS_TOKEN);
            cb.setOAuthAccessTokenSecret(ACCESS_SECRET);
            cb.setOAuthConsumerKey(CONSUMER_KEY);
            cb.setOAuthConsumerSecret(CONSUMER_SECRET);
            cb.setDebugEnabled(true);

            Twitter twitter = new TwitterFactory(cb.build()).getInstance();

            Query query = new Query("Ocean");
            query.setCount(30);

            try {
                return twitter.search(query);
            }
            catch (TwitterException te) {
                Log.w("twitterAsyncSearch", "enters exception case");
                te.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(QueryResult searchResult) {
            super.onPostExecute(searchResult);
            tweetList = searchResult.getTweets();
            addTextList(tweetList);
        }

    }

    private void twitterStream() {
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setOAuthAccessToken(ACCESS_TOKEN);
        cb.setOAuthAccessTokenSecret(ACCESS_SECRET);
        cb.setOAuthConsumerKey(CONSUMER_KEY);
        cb.setOAuthConsumerSecret(CONSUMER_SECRET);
        cb.setDebugEnabled(true);

        StatusListener listener = new StatusListener() {

            public void onStatus(Status status) {
                if ((status.getPlace() != null) && (status.getGeoLocation() !=null)) {
                    String tweet = status.getUser().getName() + " : " + status.getText();
                }
            }
            public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
            }
            public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
            }
            public void onException(Exception ex) {
                ex.printStackTrace();
            }
            @Override
            public void onScrubGeo(long arg0, long arg1) { // do nothing
            }
            @Override
            public void onStallWarning(StallWarning arg0) { // do nothing
            }

        };

        TwitterStream twitterStream = new TwitterStreamFactory(cb.build()).getInstance();

        FilterQuery fq = new FilterQuery();
        String keywords[] = {"October", "#hi"};

        fq.track(keywords);
        twitterStream.addListener(listener);
        twitterStream.filter(fq);
    }


    private void addTextList(List<Status> tweets) {

        for(int i =0; i < textList.length; ++i) {

            if (tweets.isEmpty())
                textList[i] = "null val";
            else
                textList[i] = "not empty";

            //textList[i] = (tweets.get(i) == null) ? "null value" : statusToString(tweets.get(i));
        }
    }

    private String statusToString(Status s) {
        String name = s.getUser().getName();
        String text = s.getText();

        return (new String(name + ": " + text));
    }

}
